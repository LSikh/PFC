# Proyecto Final de Carrera - Lautaro Sikh

### UNL Maps ![icono](/Imagenes/icon.png)

El presente es el proyecto final de carrera de Lautaro Sikh, de la carrera Ingeniería en Informatica.
El nombre tentativo del mismo es UNL Maps. Consta de una aplicación para dispositivos móviles bajo el SO Android
que ayuda al usuario a ubicar y dirigirse a las dependencias de la Universidad Nacional del Litoral seleccionada.
Dicha infromación se obtiene de un WebService desarrollado como parte de este proyecto.

A continuación se dejan las especificaciones técnicas del proyecto e información en general.


## Resumen

Este proyecto consta de realizar una aplicación para dispositivos móviles bajo la
tecnología Android, que a partir de los sensores de ubicación de los mismos y la conexión
a Internet, ubique al usuario en alguna de las dependencias de la UNL apoyado en la
visualización de Google Maps y le muestre el camino hacia un aula, oficina, biblioteca,
baño, edificio u otros puntos de interés que esté buscando. Una vez ubicada la posición
física del usuario, se armará el camino entre él y el punto de destino para ser mostrado en
Google Maps interactuando con su API. Para complementar la visualización se utilizarán
los planos cartográficos de los edificios en sus diferentes plantas e imágenes de los
lugares en donde están los nodos por donde se trazó el camino.
Con el objetivo de independizar los datos que representan la lógica del problema
de la aplicación, se desarrollará un Web Service donde se podrán consumir las distintas
categorías de búsqueda, pero que también será el encargado de calcular el camino
óptimo por el que debe pasar el usuario.


## Especificaciones Técnicas

El proyecto consta de dos partes. Por un lado, está la aplicación desarrollada en Android que implementa la API de Google Maps.
Esta API se combina con los sensores de ubicación del telefono que nos permite ubicar la posición del mismo y mostrarla en un 
mapa.
Pero ademas, la API provee de otros elementos como son las Polilineas y los GroundsOverlays. Los primeros permiten trazar lineas
en una mapa en función de una sucesión de marcadores. La segunda permite colocar imagenes por encima del mapa que en nuestro caso serán
los planos de los edificios.

![Pantalla](/Imagenes/Demo1.png)


Por otro lado, en el servidor, corre un Rest Web Service del cual consume el móvil.
El mismo tiene implementado distintos controladores que atienden las peticiones que le llega de aplicación y envia una respuesta en formato JSON.
En él están implementados los DAOs y Servicios correspondientes para recuperar la información de la base de datos.


### Tecnologías

* [Eclipse Oxygen] - IDE Java
* [Maven] - Gestor de proyectos y dependencias
* [MySQL] - Gestor de bases de datos
* [Tomcat Server] - Servidor de aplicaciones
* [Hibernate] - Mapeo Objeto Relación
* [Spring] - Contenedor de inversión de control
* [Android Studio] - IDE Android
* [GitLab] - Controlador de versiones
* [JSON] - Intercambio de datos


[Eclipse Oxygen]: <https://www.eclipse.org/>
[Maven]: <https://maven.apache.org/>
[MySQL]: <https://www.mysql.com/>
[Tomcat Server]: <https://tomcat.apache.org/download-80.cgi>
[Hibernate]: <http://hibernate.org/>
[Spring]: <https://spring.io/>
[Android Studio]: <https://developer.android.com/studio/index.html?hl=es-419>
[GitLab]: <https://gitlab.com>
[JSON]: <https://www.json.org/json-es.html>
