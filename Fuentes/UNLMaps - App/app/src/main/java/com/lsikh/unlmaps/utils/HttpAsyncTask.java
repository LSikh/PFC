package com.lsikh.unlmaps.utils;

import android.os.AsyncTask;
import android.util.Log;

import com.lsikh.unlmaps.interfaces.HttpAsyncTaskInterface;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;


public class HttpAsyncTask extends AsyncTask<String,Void,String> {

    public HttpAsyncTaskInterface httpAsyncTaskInterface;

    private Integer mode;

    public HttpAsyncTask(Integer mode) {
        this.httpAsyncTaskInterface = null;
        this.mode = mode;
    }

    @Override
    protected String doInBackground(String... url) {
        return GET(url[0]);
    }

    @Override
    protected void onPostExecute(String result){
        if(mode.equals(0)){
            httpAsyncTaskInterface.populateTipoDependenciaSpinner(result);
        } else if (mode.equals(1)){
            httpAsyncTaskInterface.populateUndiadAcademicaSpinner(result);
        } else if (mode.equals(2)){
            httpAsyncTaskInterface.populatePuntoInteresSpinner(result);
        } else if(mode.equals(3)){
            httpAsyncTaskInterface.getPath(result);
        } else if(mode.equals(4) || mode.equals(5)){
            httpAsyncTaskInterface.getPoints(result);
        }
        try {
            this.finalize();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    public static String GET (String url) {
        InputStream inputStream = null;
        String result = "";
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse httpResponse = httpClient.execute(new HttpGet(url));
            inputStream = httpResponse.getEntity().getContent();
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "No funciona!";
        }
        catch(Exception e){
            Log.d("InputStream",e.getLocalizedMessage());
        }
        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }

    public HttpAsyncTaskInterface getHttpAsyncTaskInterface() {
        return httpAsyncTaskInterface;
    }

    public void setHttpAsyncTaskInterface(HttpAsyncTaskInterface httpAsyncTaskInterface) {
        this.httpAsyncTaskInterface = httpAsyncTaskInterface;
    }

    public Integer getMode() {
        return mode;
    }

    public void setMode(Integer mode) {
        this.mode = mode;
    }


}

