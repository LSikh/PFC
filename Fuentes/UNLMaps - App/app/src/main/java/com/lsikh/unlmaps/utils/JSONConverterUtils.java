package com.lsikh.unlmaps.utils;

import com.lsikh.unlmaps.model.PuntoDto;
import com.lsikh.unlmaps.model.PuntoInteresDto;
import com.lsikh.unlmaps.model.TipoDependenciaDto;
import com.lsikh.unlmaps.model.UnidadAcademicaDto;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JSONConverterUtils {

    private final static String JSON_ARRAY_NAME = "response";

    public final static String SPINNER_PROMPT = "Seleccione uno";


    public static List<UnidadAcademicaDto> JSONUnidadAcademicaConverter(String jsonString){
        List<UnidadAcademicaDto> items = new ArrayList<UnidadAcademicaDto>();
        UnidadAcademicaDto emptyDto = new UnidadAcademicaDto();
        emptyDto.setNombre(SPINNER_PROMPT);
        emptyDto.setId(0);
        items.add(emptyDto);
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONArray jsonArray = jsonObject.getJSONArray(JSON_ARRAY_NAME);
            for(int index = 0; index < jsonArray.length(); index++){
                UnidadAcademicaDto dto = new UnidadAcademicaDto();
                dto.setId(jsonArray.getJSONObject(index).getInt("id"));
                dto.setNombre(jsonArray.getJSONObject(index).getString("nombre"));
                items.add(dto);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return items;
    }

    public static List<TipoDependenciaDto> JSONTipoDependenciaConverter(String jsonString){
        List<TipoDependenciaDto> items = new ArrayList<TipoDependenciaDto>();
        TipoDependenciaDto emptyDto = new TipoDependenciaDto();
        emptyDto.setDescripcion(SPINNER_PROMPT);
        emptyDto.setId(0);
        items.add(emptyDto);
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONArray jsonArray = jsonObject.getJSONArray(JSON_ARRAY_NAME);
            for(int index = 0; index < jsonArray.length(); index++){
                TipoDependenciaDto dto = new TipoDependenciaDto();
                dto.setId(jsonArray.getJSONObject(index).getInt("id"));
                dto.setDescripcion(jsonArray.getJSONObject(index).getString("descripcion"));
                items.add(dto);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return items;
    }

    public static List<PuntoInteresDto> JSONPuntoInteresConverter(String jsonString){
        List<PuntoInteresDto> items = new ArrayList<PuntoInteresDto>();
        PuntoInteresDto emptyDto = new PuntoInteresDto();
        emptyDto.setNombre(SPINNER_PROMPT);
        emptyDto.setId(0);
        items.add(emptyDto);
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONArray jsonArray = jsonObject.getJSONArray(JSON_ARRAY_NAME);
            for(int index = 0; index < jsonArray.length(); index++){
                PuntoInteresDto dto = new PuntoInteresDto();
                dto.setId(jsonArray.getJSONObject(index).getInt("id"));
                dto.setNombre(jsonArray.getJSONObject(index).getString("nombre"));
                items.add(dto);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return items;
    }

    public static List<PuntoDto> JSONPuntoConverter(String jsonString){
        List<PuntoDto> items = new ArrayList<PuntoDto>();
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONArray jsonArray = jsonObject.getJSONArray(JSON_ARRAY_NAME);
            for(int index = 0; index < jsonArray.length(); index++){
                PuntoDto dto = new PuntoDto();
                dto.setId(jsonArray.getJSONObject(index).getInt("id"));
                dto.setNombre(jsonArray.getJSONObject(index).getString("nombre"));
                dto.setIdTipoDependencia(jsonArray.getJSONObject(index).getInt("idTipoDependencia"));
                dto.setIdUnidadAcademica(jsonArray.getJSONObject(index).getInt("idUnidadAcademica"));
                dto.setIdEdificio(jsonArray.getJSONObject(index).getInt("idEdificio"));
                dto.setPiso(jsonArray.getJSONObject(index).getInt("piso"));
                dto.setImagen(jsonArray.getJSONObject(index).getBoolean("tieneImagen"));
                dto.setLatitud(jsonArray.getJSONObject(index).getDouble("latitud"));
                dto.setLongitud(jsonArray.getJSONObject(index).getDouble("longitud"));
                items.add(dto);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return items;
    }
}
