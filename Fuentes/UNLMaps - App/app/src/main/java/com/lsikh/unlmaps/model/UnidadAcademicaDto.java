package com.lsikh.unlmaps.model;

/**
 * Created by lsikh on 22/02/2018.
 */

public class UnidadAcademicaDto {

    private Integer id;

    private String nombre;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return getNombre();
    }
}
