package com.lsikh.unlmaps.model;

public class PuntoDto {

    private Integer id;

    private Double latitud;

    private Double longitud;

    private Integer piso;

    private String nombre;

    private Integer idTipoDependencia;

    private Integer idUnidadAcademica;

    private Integer idEdificio;

    private Boolean imagen;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public Integer getPiso() {
        return piso;
    }

    public void setPiso(Integer piso) {
        this.piso = piso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getIdTipoDependencia() {
        return idTipoDependencia;
    }

    public void setIdTipoDependencia(Integer idTipoDependencia) {
        this.idTipoDependencia = idTipoDependencia;
    }

    public Integer getIdUnidadAcademica() {
        return idUnidadAcademica;
    }

    public void setIdUnidadAcademica(Integer idUnidadAcademica) {
        this.idUnidadAcademica = idUnidadAcademica;
    }

    public Boolean getImagen() {
        return imagen;
    }

    public void setImagen(Boolean imagen) {
        this.imagen = imagen;
    }

    public Integer getIdEdificio() {
        return idEdificio;
    }

    public void setIdEdificio(Integer idEdificio) {
        this.idEdificio = idEdificio;
    }

}
