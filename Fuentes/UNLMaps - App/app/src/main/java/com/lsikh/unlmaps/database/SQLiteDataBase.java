package com.lsikh.unlmaps.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteDataBase extends SQLiteOpenHelper {

    private static final String DROP_TABLE_BUSQUEDAS = "DROP TABLE IF EXISTS Busquedas";

    private static final String CREATE_TABLE_BUSQUEDAS = "CREATE TABLE Busquedas (d_tipo TEXT, c_tipo INTEGER, d_unidad TEXT, c_unidad INTEGER, d_punto TEXT, c_punto INTEGER)";

    public static final Integer CURRENT_DATABASE_VERSION = 1;

    public SQLiteDataBase(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DROP_TABLE_BUSQUEDAS);
        sqLiteDatabase.execSQL(CREATE_TABLE_BUSQUEDAS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(DROP_TABLE_BUSQUEDAS);
        sqLiteDatabase.execSQL(CREATE_TABLE_BUSQUEDAS);
    }
}
