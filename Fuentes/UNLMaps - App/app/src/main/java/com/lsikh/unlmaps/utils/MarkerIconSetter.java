package com.lsikh.unlmaps.utils;

import com.lsikh.unlmaps.R;

public class MarkerIconSetter {

    public static Integer setIcon(Integer idTipoDependencia, Integer idUnidadAcademica) {
        switch (idTipoDependencia) {
            case 1:
                return R.drawable.markers_icon_classroom;
            case 2:
                return R.drawable.markers_icon_bathroom;
            case 3:
                return R.drawable.markers_icon_bar;
            case 4:
                return R.drawable.markers_icon_building;
            case 5:
                return R.drawable.markers_icon_ladder;
            case 6:
                return R.drawable.markers_icon_copy;
            case 7:
                if (idUnidadAcademica.equals(1)) {
                    return R.drawable.markers_icon_computer_lab;
                } else {
                    return R.drawable.markers_icon_lab;
                }
            case 8:
                return R.drawable.markers_icon_offices;
            case 9:
                return R.drawable.markers_icon_others;
            case 10:
                return R.drawable.markers_icon_work;
            case 12:
                return R.drawable.markers_icon_library;
            default:
                return R.drawable.markers_icon_location;

        }
    }

}
