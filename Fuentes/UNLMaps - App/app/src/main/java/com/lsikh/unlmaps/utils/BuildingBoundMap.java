package com.lsikh.unlmaps.utils;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.lsikh.unlmaps.R;

import java.util.HashMap;
import java.util.Map;

public class BuildingBoundMap {

    private Map<String, LatLngBounds> boundsMap = new HashMap<>();

    private Map<String, Integer> idsMap = new HashMap<>();

    public static Integer buildingsCount = 5;

    public BuildingBoundMap() {
        //Edificio 2 - FICH/FBCB
       /* boundsMap.put("ed2_0", new LatLngBounds(new LatLng(-31.640064, -60.673090), new LatLng(-31.639671, -60.671973)));
        boundsMap.put("ed2_1", new LatLngBounds(new LatLng(-31.640064, -60.673090), new LatLng(-31.639671, -60.671973)));
        boundsMap.put("ed2_2", new LatLngBounds(new LatLng(-31.640064, -60.673090), new LatLng(-31.639671, -60.671973)));
        boundsMap.put("ed2_3", new LatLngBounds(new LatLng(-31.640064, -60.673090), new LatLng(-31.639671, -60.671973)));*/

        boundsMap.put("ed2_0", new LatLngBounds(new LatLng(-31.640064, -60.673090), new LatLng(-31.63967462946029, -60.67197896540166)));
        boundsMap.put("ed2_1", new LatLngBounds(new LatLng(-31.640064, -60.673090), new LatLng(-31.63967462946029, -60.67197896540166)));
        boundsMap.put("ed2_2", new LatLngBounds(new LatLng(-31.640064, -60.673090), new LatLng(-31.63967462946029, -60.67197896540166)));
        boundsMap.put("ed2_3", new LatLngBounds(new LatLng(-31.640064, -60.673090), new LatLng(-31.63967462946029, -60.67197896540166)));

        //Edificio 3 - FCM
        boundsMap.put("ed3_0", new LatLngBounds(new LatLng(-31.639872, -60.670817), new LatLng(-31.639313, -60.670216)));

        //Edificio 4 - NAVE FICH
        boundsMap.put("ed4_0", new LatLngBounds(new LatLng(-31.639896, -60.671735), new LatLng(-31.639576, -60.670991)));
        boundsMap.put("ed4_1", new LatLngBounds(new LatLng(-31.639896, -60.671735), new LatLng(-31.639576, -60.670991)));

        //Edificio 5 - FADU/FHUC
        boundsMap.put("ed5_0", new LatLngBounds(new LatLng(-31.640346, -60.673949), new LatLng(-31.639980, -60.673311)));
        boundsMap.put("ed5_1", new LatLngBounds(new LatLng(-31.640346, -60.673949), new LatLng(-31.639980, -60.673311)));
        boundsMap.put("ed5_2", new LatLngBounds(new LatLng(-31.640346, -60.673949), new LatLng(-31.639980, -60.673311)));
        boundsMap.put("ed5_3", new LatLngBounds(new LatLng(-31.640346, -60.673949), new LatLng(-31.639980, -60.673311)));
        boundsMap.put("ed5_4", new LatLngBounds(new LatLng(-31.640346, -60.673949), new LatLng(-31.639980, -60.673311)));

        //Edificio 6 - AULARIO
        boundsMap.put("ed6_0", new LatLngBounds(new LatLng(-31.640131, -60.674323), new LatLng(-31.639922, -60.674045)));
        boundsMap.put("ed6_1", new LatLngBounds(new LatLng(-31.640131, -60.674323), new LatLng(-31.639922, -60.674045)));
        boundsMap.put("ed6_2", new LatLngBounds(new LatLng(-31.640131, -60.674323), new LatLng(-31.639922, -60.674045)));
        boundsMap.put("ed6_3", new LatLngBounds(new LatLng(-31.640131, -60.674323), new LatLng(-31.639922, -60.674045)));
        boundsMap.put("ed6_4", new LatLngBounds(new LatLng(-31.640131, -60.674323), new LatLng(-31.639922, -60.674045)));
        boundsMap.put("ed6_5", new LatLngBounds(new LatLng(-31.640131, -60.674323), new LatLng(-31.639922, -60.674045)));


        //--------------------------------------------------------------------------------------------------------------------//

        idsMap.put("ed2_0", R.drawable.ed2_0);

        idsMap.put("ed2_1", R.drawable.ed2_1);

        idsMap.put("ed2_2", R.drawable.ed2_2);

        idsMap.put("ed2_3", R.drawable.ed2_3);

        idsMap.put("ed3_0", R.drawable.ed3_0);

        idsMap.put("ed4_0", R.drawable.ed4_0);

        idsMap.put("ed4_1", R.drawable.ed4_1);

        idsMap.put("ed5_0", R.drawable.ed5_0);

        idsMap.put("ed5_1", R.drawable.ed5_1);

        idsMap.put("ed5_2", R.drawable.ed5_2);

        idsMap.put("ed5_3", R.drawable.ed5_3);

        idsMap.put("ed5_4", R.drawable.ed5_4);

        idsMap.put("ed6_0", R.drawable.ed6_0);

        idsMap.put("ed6_1", R.drawable.ed6_1);

        idsMap.put("ed6_2", R.drawable.ed6_2);

        idsMap.put("ed6_3", R.drawable.ed6_3);

        idsMap.put("ed6_4", R.drawable.ed6_4);

        idsMap.put("ed6_5", R.drawable.ed6_5);

        //--------------------------------------------------------------------------------------------------------------------//
    }

    public Map<String, LatLngBounds> getBoundsMap() {
        return boundsMap;
    }

    public Map<String, Integer> getIdsMap() {
        return idsMap;
    }
}
