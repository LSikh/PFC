package com.lsikh.unlmaps;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.lsikh.unlmaps.barcodescanner.IntentIntegrator;
import com.lsikh.unlmaps.barcodescanner.IntentResult;
import com.lsikh.unlmaps.fragments.LastSearchsFragment;
import com.lsikh.unlmaps.fragments.MapsFragment;
import com.lsikh.unlmaps.fragments.SearchFragment;
import com.lsikh.unlmaps.model.PuntoDto;

import org.apache.commons.lang3.BooleanUtils;

import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final int INITIAL_REQUEST = 1337;

    private static final String[] INITIAL_PERMS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.CAMERA,
            Manifest.permission.INTERNET
    };

    private MapsFragment mapsFragment = new MapsFragment();

    private SearchFragment searchFragment = new SearchFragment();

    private FragmentManager fragmentManager = getSupportFragmentManager();

    private Menu menu;

    private FloatingActionButton qrButton;

    private IntentIntegrator scanIntegrator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(INITIAL_PERMS, INITIAL_REQUEST);
        }

        scanIntegrator = new IntentIntegrator(this);

        qrButton = (FloatingActionButton) findViewById(R.id.fab);
        qrButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanIntegrator.initiateScan();
            }
        });

        searchFragment.setMainActivity(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        fragmentManager.beginTransaction().replace(R.id.fragment_container, mapsFragment).addToBackStack(null).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (fragmentManager.findFragmentById(R.id.fragment_container) instanceof MapsFragment) {
                finish();
            } else {
                fragmentManager.popBackStack();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.main, menu);
        for (int index = 0; index < menu.size(); index++) {
            menu.getItem(index).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        boolean isChecked;

        for (int index = 0; index < menu.size(); index++) {
            menu.getItem(index).setChecked(false);
        }
        if (mapsFragment.getPolilineMode()) {
            if (id == R.id.radioButton0) {
                isChecked = !item.isChecked();
                item.setChecked(isChecked);
                mapsFragment.changePoliline(0);
                return true;
            } else if (id == R.id.radioButton1) {
                isChecked = !item.isChecked();
                item.setChecked(isChecked);
                mapsFragment.changePoliline(1);
                return true;
            } else if (id == R.id.radioButton2) {
                isChecked = !item.isChecked();
                item.setChecked(isChecked);
                mapsFragment.changePoliline(2);
                return true;
            } else if (id == R.id.radioButton3) {
                isChecked = !item.isChecked();
                item.setChecked(isChecked);
                mapsFragment.changePoliline(3);
                return true;
            } else if (id == R.id.radioButton4) {
                isChecked = !item.isChecked();
                item.setChecked(isChecked);
                mapsFragment.changePoliline(4);
                return true;
            } else if (id == R.id.radioButton5) {
                isChecked = !item.isChecked();
                item.setChecked(isChecked);
                mapsFragment.changePoliline(5);
                return true;
            }
        } else {
            if (id == R.id.radioButton0) {
                isChecked = !item.isChecked();
                item.setChecked(isChecked);
                mapsFragment.changeNodes(0);
                return true;
            } else if (id == R.id.radioButton1) {
                isChecked = !item.isChecked();
                item.setChecked(isChecked);
                mapsFragment.changeNodes(1);
                return true;
            } else if (id == R.id.radioButton2) {
                isChecked = !item.isChecked();
                item.setChecked(isChecked);
                mapsFragment.changeNodes(2);
                return true;
            } else if (id == R.id.radioButton3) {
                isChecked = !item.isChecked();
                item.setChecked(isChecked);
                mapsFragment.changeNodes(3);
                return true;
            } else if (id == R.id.radioButton4) {
                isChecked = !item.isChecked();
                item.setChecked(isChecked);
                mapsFragment.changeNodes(4);
                return true;
            } else if (id == R.id.radioButton5) {
                isChecked = !item.isChecked();
                item.setChecked(isChecked);
                mapsFragment.changeNodes(5);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        searchFragment.setMainActivity(this);
        int id = item.getItemId();
        if (id == R.id.nav_busqueda) {
            if (!(fragmentManager.findFragmentById(R.id.fragment_container) instanceof SearchFragment)) {
                qrButton.hide();
                menu.clear();
                //fm.popBackStack();
                fragmentManager.beginTransaction().replace(R.id.fragment_container, searchFragment).addToBackStack(null).commit();
            }
        } else if (id == R.id.nav_mapa) {
            if (!(fragmentManager.findFragmentById(R.id.fragment_container) instanceof MapsFragment)) {
                fragmentManager.beginTransaction().replace(R.id.fragment_container, mapsFragment).addToBackStack(null).commit();
            }
        } //else if (id == R.id.nav_busquedas_guardadas) {

        //}
        else if (id == R.id.nav_ultimas_busquedas) {
            if (!(fragmentManager.findFragmentById(R.id.fragment_container) instanceof LastSearchsFragment)) {
                qrButton.hide();
                menu.clear();
                LastSearchsFragment lastSearchsFragment = new LastSearchsFragment();
                lastSearchsFragment.setMainActivity(this);
                //fm.popBackStack();
                fragmentManager.beginTransaction().replace(R.id.fragment_container, lastSearchsFragment).addToBackStack(null).commit();
            }
        } else if (id == R.id.nav_info) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void showSearchResultOnMap(List<PuntoDto> points, Boolean isPath) {
        if (isPath) {
            mapsFragment.showPath(points);
            String message = getString(R.string.main_activity_on_result_toast);
            String floor = new String();
            switch(points.get(points.size() - 1).getPiso()){
                case 0:
                    floor = new String(getString(R.string.radio_group_planta_baja));
                    break;
                case 1:
                    floor = new String(getString(R.string.radio_group_primer_piso));
                    break;
                case 2:
                    floor = new String(getString(R.string.radio_group_segundo_piso));
                    break;
                case 3:
                    floor = new String(getString(R.string.radio_group_tercer_piso));
                    break;
                case 4:
                    floor = new String(getString(R.string.radio_group_cuarto_piso));
                    break;
                case 5:
                    floor = new String(getString(R.string.radio_group_quinto_piso));
                    break;
            }
            message = message.concat(" " + floor);
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        } else {
            mapsFragment.showNodes(points);
        }
        setVisibleMenuItems(mapsFragment.getFloorsToShow());
        qrButton.show();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, mapsFragment).addToBackStack(null).commit();
    }

    public void setVisibleMenuItems(Integer floors) {
        getMenuInflater().inflate(R.menu.main, menu);
        for (Integer index = 0; index < 6; index++) {
            if (index < floors) {
                menu.getItem(index).setVisible(true);
            } else {
                menu.getItem(index).setVisible(false);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) ||
                    !(checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
                finish();
            } else {
                mapsFragment = new MapsFragment();
                fragmentManager.beginTransaction().replace(R.id.fragment_container, mapsFragment).addToBackStack(null).commit();
            }
        }
    }

    public MapsFragment getMapsFragment() {
        return mapsFragment;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null) {
            String scanContent = scanningResult.getContents();
            String[] values = scanContent.split(",");
            //QR Format = Latitud,Longitud,Piso
            mapsFragment.setLatitude(Double.parseDouble(values[0]));
            mapsFragment.setLongitude(Double.parseDouble(values[1]));
            mapsFragment.setActualFloor(Integer.parseInt(values[2]));
            mapsFragment.updatePosition();

            //Actualizo el menu de pisos cuando cambio el piso por QR
            if(BooleanUtils.isTrue(mapsFragment.getPolilineMode())){
                mapsFragment.changePoliline(Integer.parseInt(values[2]));
            }
            else if(BooleanUtils.isFalse(mapsFragment.getPolilineMode())){
                mapsFragment.changeNodes(Integer.parseInt(values[2]));
            }
        } else {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No se ha recibido datos del scaneo!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

}
