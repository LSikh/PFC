package com.lsikh.unlmaps.enums;

import com.lsikh.unlmaps.R;

public enum PisoEnum {

    PB(0, R.string.radio_group_planta_baja),

    P1(1, R.string.radio_group_primer_piso),

    P2(2, R.string.radio_group_segundo_piso),

    P3(3, R.string.radio_group_tercer_piso),

    P4(4, R.string.radio_group_cuarto_piso),

    P5(5, R.string.radio_group_quinto_piso);

    private Integer piso;

    private Integer recurso;

    PisoEnum(Integer piso, Integer recurso) {
        this.piso = piso;
        this.recurso = recurso;
    }

    public static PisoEnum getByRecurso(Integer recurso) {
        for (PisoEnum piso : PisoEnum.values()) {
            if (piso.getRecurso().equals(recurso)) {
                return piso;
            }
        }
        return null;
    }

    public Integer getId() {
        return piso;
    }

    public Integer getRecurso() {
        return recurso;
    }
}
