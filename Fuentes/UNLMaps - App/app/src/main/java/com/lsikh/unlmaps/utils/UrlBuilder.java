package com.lsikh.unlmaps.utils;

public class UrlBuilder {


    private final static String PORT = "8080";

    private final static String BASE_URL = "http://192.168.1.123:" + PORT + "/UNLMaps";

    private final static String TIPO_DEPENDENCIA_CONTROLER = "/tipoDependencia";

    private final static String UNIDAD_ACADEMICA_CONRTOLLER = "/unidadAcademica";

    private final static String DEPENDENCIA_CONTROLLER = "/dependencias";

    private final static String CAMINO_CONTROLLER = "/camino";

    private final static String DEPENENCIA_POR_TIPO_UNIDAD = "/getDependencias?unidad=%s&tipo=%s";

    private final static String DEPENENCIA_POR_TIPO = "/getDependenciasPorTipo?tipo=%s";

    private final static String CAMINO_GET_CAMINO = "/getCamino?latActual=%s&lonActual=%s&piso=%s&id=%s";

    private final static String DEPENDENCIA_GET_EDIFICIO = "/getEdificio?tipo=%s&unidad=%s";

    private final static String DEPENDENCIA_GET_IMAGEN = "/getImagen?id=%s";

    public static String getUrlTipoDepenenciaGetAll(){
        return BASE_URL+TIPO_DEPENDENCIA_CONTROLER+"/getAll";
    }

    public static String getUrlUnidadAcademicaGetAll(){
        return BASE_URL+UNIDAD_ACADEMICA_CONRTOLLER+"/getAll";
    }

    public static String getUrlDependenciaPorTipoUnidad(String unidad, String tipo){
        return BASE_URL+DEPENDENCIA_CONTROLLER+String.format(DEPENENCIA_POR_TIPO_UNIDAD, unidad, tipo);
    }

    public static String getUrlDependenciaPorTipo(Integer tipo){
        return BASE_URL+DEPENDENCIA_CONTROLLER+String.format(DEPENENCIA_POR_TIPO, tipo);
    }

    public static String getUrlGetCamino(Double latActual, Double lonActual, Integer piso, Integer idObjetivo){
        return BASE_URL+CAMINO_CONTROLLER+String.format(CAMINO_GET_CAMINO, latActual, lonActual, piso, idObjetivo);
    }

    public static String getUrlGetAllByTipoDependenia(Integer tipo){
        return BASE_URL+DEPENDENCIA_CONTROLLER+String.format(DEPENENCIA_POR_TIPO, tipo);
    }

    public static String getUrlGetEdificio(Integer tipo, Integer unidadAcademica){
        return BASE_URL+DEPENDENCIA_CONTROLLER+String.format(DEPENDENCIA_GET_EDIFICIO, tipo, unidadAcademica);
    }

    public static String getUrlGetImagen(Integer id){
        return BASE_URL+DEPENDENCIA_CONTROLLER+String.format(DEPENDENCIA_GET_IMAGEN, id);
    }
}
