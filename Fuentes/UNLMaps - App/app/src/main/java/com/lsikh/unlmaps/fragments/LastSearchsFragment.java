package com.lsikh.unlmaps.fragments;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.lsikh.unlmaps.MainActivity;
import com.lsikh.unlmaps.R;
import com.lsikh.unlmaps.database.SQLiteDataBase;


public class LastSearchsFragment extends Fragment {

    private SQLiteDataBase sqLiteDataBase;

    private MainActivity mainActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        sqLiteDataBase = new SQLiteDataBase(getActivity(), "DB", null, SQLiteDataBase.CURRENT_DATABASE_VERSION);
        final View rootView = inflater.inflate(R.layout.fragment_last_searchs, container, false);
        final SQLiteDatabase dataBase = sqLiteDataBase.getReadableDatabase();
        Cursor cursor = dataBase.rawQuery("SELECT d_tipo, d_unidad, d_punto FROM Busquedas",null);
        cursor.moveToFirst();
        String[] arrayAdapter;
        final ListView listView = (ListView) rootView.findViewById(R.id.listView);
        if(cursor.getCount() > 0) {
            arrayAdapter = new String[cursor.getCount()];
            int index = 0;
            do {
                String tipo = cursor.getString(0);
                String unidad = cursor.getString(1);
                String punto = cursor.getString(2);
                String result = tipo;
                if(unidad != null){
                    result = result.concat(" - " + unidad);
                }
                if(punto != null){
                    result = result.concat(" - " + punto);
                }
                arrayAdapter[index] = result;
                index++;
            } while (cursor.moveToNext());

            //ItemClickListener para los elementos del listView, para hacer una busqueda desde aquí
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    String[] itemClick = listView.getItemAtPosition(i).toString().split(" - ");
                    String tipo = itemClick[0];
                    String select = new String("SELECT c_tipo, c_unidad, c_punto FROM Busquedas WHERE d_tipo = '" + tipo + "'");
                    String unidad = null;
                    String punto = null;
                    Integer idUnidadAcademica = null;
                    Integer idPunto = null;
                    if(itemClick.length > 1){
                        unidad = itemClick[1];
                        select = select.concat(" AND d_unidad = '" + unidad + "'");

                    }
                    if(itemClick.length > 2){
                        punto = itemClick[2];
                        select = select.concat(" AND d_punto like '" + punto + "%'");
                    }
                    Cursor cursor = dataBase.rawQuery(select, null);
                    cursor.moveToFirst();
                    Integer idTipoDependencia = cursor.getInt(0);
                    if(itemClick.length > 1){
                        idUnidadAcademica = cursor.getInt(1);
                    }
                    if(itemClick.length > 2){
                        idPunto = cursor.getInt(2);
                    }

                    SearchFragment searchFragment = new SearchFragment();
                    searchFragment.executeSearch(idTipoDependencia, idUnidadAcademica, idPunto,true, mainActivity);
                }
            });
        }
        else{
            arrayAdapter = new String[] {getString(R.string.fragment_last_searchs_empty_database)};
        }
        ListAdapter adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, arrayAdapter);
        listView.setAdapter(adapter);

        return rootView;
    }


    public MainActivity getMainActivity() {
        return mainActivity;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }


}
