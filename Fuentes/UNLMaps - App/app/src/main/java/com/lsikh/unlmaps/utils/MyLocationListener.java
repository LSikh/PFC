package com.lsikh.unlmaps.utils;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.lsikh.unlmaps.fragments.MapsFragment;

public class MyLocationListener implements LocationListener {

    private MapsFragment mapsFragment;

    @Override
    public void onLocationChanged(Location location) {
        mapsFragment.setLatitude(location.getLatitude());

        mapsFragment.setLongitude(location.getLongitude());

        if(mapsFragment.getMyMap() == null) {
            mapsFragment.onMapReady(mapsFragment.getMyMap());
        }
        else{
            if(Math.abs(location.getLatitude() - mapsFragment.getLatitude() + location.getLongitude() - mapsFragment.getLongitude()) < 0.00025) {   //Revisar esto
                mapsFragment.updatePosition();
            }
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    public void setMapsFragment(MapsFragment mapsFragment) {
        this.mapsFragment = mapsFragment;
    }
}
