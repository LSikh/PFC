package com.lsikh.unlmaps.interfaces;


import com.lsikh.unlmaps.model.PuntoDto;

import java.util.List;

public interface HttpAsyncTaskInterface {

    void populateTipoDependenciaSpinner(String result);

    void populateUndiadAcademicaSpinner(String result);

    void populatePuntoInteresSpinner(String result);

    void getPath(String result);

    void getPoints(String result);

}
