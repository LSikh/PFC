package com.lsikh.unlmaps.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.lsikh.unlmaps.R;
import com.lsikh.unlmaps.model.PuntoDto;
import com.lsikh.unlmaps.utils.BuildingBoundMap;
import com.lsikh.unlmaps.utils.MarkerIconSetter;
import com.lsikh.unlmaps.utils.MyLocationListener;
import com.lsikh.unlmaps.utils.UrlBuilder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class MapsFragment extends Fragment implements OnMapReadyCallback, SensorEventListener {

    private static View view;

    private GoogleMap myMap = null;

    private MyLocationListener myLocationListener = new MyLocationListener();

    private SensorManager mySensorManager = null;

    private MarkerOptions myPosition = null;

    private Marker myPositionMarker = null;

    private Double latitude = Double.valueOf(0);

    private Double longitude = Double.valueOf(0);

    private Integer actualFloor = 0;

    private Integer floorsToShow = 0;

    private Vector<PolylineOptions> polilines = new Vector<>();

    private Vector<Vector<MarkerOptions>> pathMarkers = new Vector<>();

    private Vector<PuntoDto> nodesMarkers = new Vector<>();

    private Vector<Vector<GroundOverlayOptions>> overlays = new Vector<>();

    private BuildingBoundMap buildingBoundMap = new BuildingBoundMap();

    private Map<String, LatLngBounds> boundsMap = buildingBoundMap.getBoundsMap();

    private Map<String, Integer> idsMap = buildingBoundMap.getIdsMap();

    private Vector<Vector<MarkerOptions>> imagesMarkers = new Vector<>();

    private List<PuntoDto> nodes;

    private Boolean polilineMode;

    private Float angle = 0.f;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_map, container, false);
        } catch (InflateException e) {

        }

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //LocationManager
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        myLocationListener.setMapsFragment(this);

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        }

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, (android.location.LocationListener) myLocationListener);

        //SensorManager
        mySensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        mySensorManager.registerListener((SensorEventListener) this, mySensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                1000000);

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        myMap = googleMap;

        myMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Log.d("LOGGER", latLng.latitude + ", " + latLng.longitude);
            }
        });

        LatLng position = new LatLng(this.latitude, this.longitude);
        myMap.moveCamera(CameraUpdateFactory.newLatLng(position));
        myMap.moveCamera(CameraUpdateFactory.zoomTo(18));
        myPosition = new MarkerOptions()
                .position(new LatLng(this.latitude, this.longitude))
                .title(getString(R.string.maps_fragment_my_marker_label))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.markers_icon_location));
        myPositionMarker = myMap.addMarker(myPosition);

        myMap.moveCamera(CameraUpdateFactory.zoomTo(18));

        //Add markers
        for (int index = 0; index < nodesMarkers.size(); index++) {
            if (nodesMarkers.elementAt(index).getPiso().equals(actualFloor)) {
                myMap.addMarker(
                        new MarkerOptions()
                                .position(new LatLng(nodesMarkers.elementAt(index).getLatitud(), nodesMarkers.elementAt(index).getLongitud()))
                                .title(nodesMarkers.elementAt(index).getNombre())
                                .icon(BitmapDescriptorFactory.fromResource(MarkerIconSetter.setIcon(nodesMarkers.elementAt(index).getIdTipoDependencia(), nodesMarkers.elementAt(index).getIdUnidadAcademica()))));
            }
        }

        //Add poliline
        if (polilines.size() != 0) {
            myMap.addPolyline(polilines.elementAt(actualFloor));
            for (int index = 0; index < pathMarkers.elementAt(actualFloor).size(); index++) {
                myMap.addMarker(pathMarkers.elementAt(actualFloor).elementAt(index));
            }
        }

        //Add overlays
        if (overlays.size() != 0) {
            for (int i = 0; i < overlays.elementAt(actualFloor).size(); i++) {
                myMap.addGroundOverlay(overlays.elementAt(actualFloor).elementAt(i));
            }
        }

        //Add overlays
        if (imagesMarkers.size() != 0) {
            for (int i = 0; i < imagesMarkers.elementAt(actualFloor).size(); i++) {
                myMap.addMarker(imagesMarkers.elementAt(actualFloor).elementAt(i));
            }
        }

        //Hago mi propia InfoWindow, para poder mostrar una imagen del nodo cuando hago click en el y ver el lugar que está señalado
        myMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                View view = getActivity().getLayoutInflater().inflate(R.layout.custom_infowindow, null);
                final ImageView image = (ImageView) view.findViewById(R.id.infowindow_imageView);
                TextView title = (TextView) view.findViewById(R.id.infowindow_title);
                if (nodes != null) {
                    for (PuntoDto puntoDto : nodes) {
                        if (marker.getPosition().latitude == puntoDto.getLatitud() && marker.getPosition().longitude == puntoDto.getLongitud()
                                && puntoDto.getImagen()) {
                            Picasso.get()
                                    .load(UrlBuilder.getUrlGetImagen(puntoDto.getId()))
                                    .centerCrop()
                                    .resize(250, 178)
                                    .into(image);
                            break;
                        }

                    }
                }
                if (nodesMarkers != null) {
                    for (PuntoDto puntoDto : nodesMarkers) {
                        if (marker.getPosition().latitude == puntoDto.getLatitud() && marker.getPosition().longitude == puntoDto.getLongitud()
                                && puntoDto.getImagen()) {
                            Picasso.get()
                                    .load(UrlBuilder.getUrlGetImagen(puntoDto.getId()))
                                    .centerCrop()
                                    .resize(250, 178)
                                    .into(image);
                            break;
                        }

                    }
                }

                title.setText(marker.getTitle());
                return view;
            }
        });
    }

    public void updatePosition() {
        LatLng position = new LatLng(this.latitude, this.longitude);
        myMap.moveCamera(CameraUpdateFactory.newLatLng(position));
        myPosition.position(position);
        myPositionMarker.remove();
        myPositionMarker = myMap.addMarker(myPosition);
    }

    public void clearAll() {
        polilines.clear();
        nodesMarkers.clear();
        pathMarkers.clear();
        overlays.clear();
        myMap.clear();
        floorsToShow = 0;
    }

    public void showPath(List<PuntoDto> path) {
        clearAll();
        nodes = new ArrayList<>(path);
        polilineMode = new Boolean(Boolean.TRUE);
        Vector<String> buildings = new Vector<>();

        for (PuntoDto point : path) {
            if (point.getPiso() > floorsToShow) {
                floorsToShow = point.getPiso();
            }
        }

        floorsToShow = floorsToShow + 1;
        for (int index = 0; index < floorsToShow; index++) {
            PolylineOptions poliline = new PolylineOptions().width(5).color(Color.RED);
            Vector<GroundOverlayOptions> groundOverlay = new Vector<>();
            polilines.add(poliline);
            overlays.add(groundOverlay);
            pathMarkers.add(new Vector<MarkerOptions>());
            imagesMarkers.add(new Vector<MarkerOptions>());
        }

        //Agrego puntos a las polilineas segun piso e identifico por que edificios y pisos pasa mi polilinea
        for (PuntoDto point : path) {
            polilines.elementAt(point.getPiso()).add(new LatLng(point.getLatitud(), point.getLongitud()));
            if (!point.getIdEdificio().equals(1)) {
                if (!buildings.contains("ed" + point.getIdEdificio() + "_" + point.getPiso())) {
                    buildings.add("ed" + point.getIdEdificio() + "_" + point.getPiso());
                }
            }

        }

        for (int index = 0; index < buildings.size(); index++) {
            if (idsMap.containsKey(buildings.elementAt(index))) {
                overlays.elementAt(Integer.parseInt(buildings.elementAt(index).substring(buildings.elementAt(index).indexOf("_") + 1)))
                        .add(new GroundOverlayOptions()
                                .positionFromBounds(boundsMap.get(buildings.elementAt(index)))
                                .image(BitmapDescriptorFactory.fromResource(idsMap.get(buildings.elementAt(index)))));
            }
        }

        //Busco cuales marcadores por piso voy a tener
        pathMarkers.elementAt(0).add(new MarkerOptions()
                .position(new LatLng(path.get(0).getLatitud(), path.get(0).getLongitud()))
                .title(path.get(0).getNombre())
                .icon(BitmapDescriptorFactory.fromResource(MarkerIconSetter.setIcon(path.get(0).getIdTipoDependencia(), path.get(0).getIdUnidadAcademica()))));

        for (int index = 1; index < path.size() - 1; index++) {
            if (path.get(index).getPiso() != path.get(index + 1).getPiso()) {
                pathMarkers.elementAt(path.get(index).getPiso()).add(new MarkerOptions()
                        .position(new LatLng(path.get(index).getLatitud(), path.get(index).getLongitud()))
                        .title(path.get(index).getNombre())
                        .icon(BitmapDescriptorFactory.fromResource(MarkerIconSetter.setIcon(path.get(index).getIdTipoDependencia(), path.get(index).getIdUnidadAcademica()))));
                pathMarkers.elementAt(path.get(index).getPiso() + 1).add(new MarkerOptions()
                        .position(new LatLng(path.get(index + 1).getLatitud(), path.get(index + 1).getLongitud()))
                        .title(path.get(index + 1).getNombre())
                        .icon(BitmapDescriptorFactory.fromResource(MarkerIconSetter.setIcon(path.get(index + 1).getIdTipoDependencia(), path.get(index + 1).getIdUnidadAcademica()))));
            } else if (path.get(index).getImagen()) {
                imagesMarkers.elementAt(path.get(index).getPiso()).add(new MarkerOptions()
                        .position(new LatLng(path.get(index).getLatitud(), path.get(index).getLongitud()))
                        .title(path.get(index).getNombre())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_icon_has_image)));
            }
        }

        pathMarkers.elementAt(floorsToShow - 1).add(new MarkerOptions()
                .position(new LatLng(path.get(path.size() - 1).getLatitud(), path.get(path.size() - 1).getLongitud()))
                .title(path.get(path.size() - 1).getNombre())
                .icon(BitmapDescriptorFactory.fromResource(MarkerIconSetter.setIcon(path.get(path.size() - 1).getIdTipoDependencia(), path.get(path.size() - 1).getIdUnidadAcademica()))));

        if(getActualFloor() != null){
            changePoliline(getActualFloor());
        }
    }

    public void changePoliline(Integer floor) {
        myMap.clear();
        myMap.addMarker(myPosition);
        myMap.addPolyline(polilines.elementAt(floor));
        for (int index = 0; index < pathMarkers.elementAt(actualFloor).size(); index++) {
            myMap.addMarker(pathMarkers.elementAt(floor).elementAt(index));
        }
        //Agrego los overlays
        if (overlays.size() > floor) {
            for (int i = 0; i < overlays.elementAt(floor).size(); i++) {
                myMap.addGroundOverlay(overlays.elementAt(floor).elementAt(i));
            }
        }

        //Agrego los overlays
        if (imagesMarkers.size() > floor) {
            for (int i = 0; i < imagesMarkers.elementAt(floor).size(); i++) {
                myMap.addMarker(imagesMarkers.elementAt(floor).elementAt(i));
            }
        }
    }

    //Recibo un conjunto de puntos y creo marcadores para todos ellos
    public void showNodes(List<PuntoDto> nodes) {
        clearAll();
        nodes = new ArrayList<>(nodes);
        polilineMode = new Boolean(Boolean.FALSE);
        Vector<String> buildings = new Vector<>();
        for (PuntoDto point : nodes) {
            if (point.getPiso() > floorsToShow) {
                floorsToShow = point.getPiso();
            }
            nodesMarkers.add(point);
            if (!point.getIdEdificio().equals(1) && !buildings.contains("ed" + point.getIdEdificio() + "_" + point.getPiso())) {
                buildings.add("ed" + point.getIdEdificio() + "_" + point.getPiso());
            }

        }
        //Creo overlays que voy a usar
        floorsToShow = floorsToShow + 1;
        for (int index = 0; index < floorsToShow; index++) {
            Vector<GroundOverlayOptions> groundOverlay = new Vector<>();
            overlays.add(groundOverlay);
        }

        //Agrego los overlays a mi vector
        for (int i = 0; i < buildings.size(); i++) {
            if (idsMap.containsKey(buildings.elementAt(i))) {
                overlays.elementAt(Integer.parseInt(buildings.elementAt(i).substring(buildings.elementAt(i).indexOf("_") + 1)))
                        .add(new GroundOverlayOptions()
                                .positionFromBounds(boundsMap.get(buildings.elementAt(i)))
                                .image(BitmapDescriptorFactory.fromResource(idsMap.get(buildings.elementAt(i)))));
            }
        }

        if(getActualFloor() != null){
            changeNodes(getActualFloor());
        }

    }

    public void changeNodes(Integer floor) {
        myMap.clear();
        myMap.addMarker(myPosition);
        for (int index = 0; index < nodesMarkers.size(); index++) {
            if (nodesMarkers.elementAt(index).getPiso().equals(floor)) {
                myMap.addMarker(new MarkerOptions()
                        .position(new LatLng(nodesMarkers.elementAt(index).getLatitud(), nodesMarkers.elementAt(index).getLongitud()))
                        .title(nodesMarkers.elementAt(index).getNombre())
                        .icon(BitmapDescriptorFactory.fromResource(MarkerIconSetter.setIcon(nodesMarkers.elementAt(index).getIdTipoDependencia(), nodesMarkers.elementAt(index).getIdUnidadAcademica()))));
            }
        }

        //Agrego los overlays
        if (overlays.size() > floor) {
            for (int index = 0; index < overlays.elementAt(floor).size(); index++) {
                myMap.addGroundOverlay(overlays.elementAt(floor).elementAt(index));
            }
        }
    }


    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public LatLng getPosition() {
        return new LatLng(this.latitude, this.longitude);
    }

    public Integer getActualFloor() {
        return actualFloor;
    }

    public void setActualFloor(Integer actualFloor) {
        this.actualFloor = actualFloor;
    }

    public MyLocationListener getMyLocationListener() {
        return myLocationListener;
    }

    public void setMyLocationListener(MyLocationListener myLocationListener) {
        this.myLocationListener = myLocationListener;
    }

    public GoogleMap getMyMap() {
        return myMap;
    }

    public void setMyMap(GoogleMap myMap) {
        this.myMap = myMap;
    }

    public Integer getFloorsToShow() {
        return floorsToShow;
    }

    public void setFloorsToShow(Integer floorsToShow) {
        this.floorsToShow = floorsToShow;
    }

    public Boolean getPolilineMode() {
        return polilineMode;
    }

    public void setPolilineMode(Boolean polilineMode) {
        this.polilineMode = polilineMode;
    }


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        synchronized (this) {
            switch (sensorEvent.sensor.getType()) {
                case Sensor.TYPE_ORIENTATION:
                    float degree = Math.round(sensorEvent.values[0]);
                    //Si el angulo de rotación con respecto a la rotación de la muestra anterior es mayor a X
                    //roto la camara, sino no porque sino baila mucho
                    if (Math.abs(degree - angle) > 30 && myMap != null) {
                        Log.d("PruebaSensor", angle + "   -    " + degree);
                        angle = degree;
                        CameraPosition oldPosition = myMap.getCameraPosition();
                        CameraPosition posistion = CameraPosition.builder(oldPosition).bearing(degree).build();
                        myMap.moveCamera(CameraUpdateFactory.newCameraPosition(posistion));
                    }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
