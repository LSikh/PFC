-- Lado FICH

INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica) 
VALUES (0, -31.640935, -60.671913, 0, 'Ciudad Universitaria', 1, 11, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica) 
VALUES (1, -31.639950, -60.671896, 0, 'Entrada FICH', 2, 1, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(2, -31.639953, -60.672090, 0, 'Aula Magna', 2, 1, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(3, -31.639962, -60.672151, 0, 'Escalera', 2, 5, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(4, -31.639965, -60.672261, 0, 'Fotocopiadora', 2, 6, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(5, -31.639965, -60.672306, 0, 'Baños', 2, 2, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(6, -31.639963, -60.6724285, 0, '', 2, 11, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(7, -31.639898,-60.672430, 0, 'Cantina', 2, 3, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(8, -31.639799,-60.672426, 0, 'Aula 4', 2, 1, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(9, -31.639799, -60.672361, 0, 'Aula Magna', 2, 1, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(10, -31.639798, -60.672181, 0, 'Aula 5 - Aula 8', 2, 1, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(11, -31.639801, -60.672471, 0, 'Aula 3', 2, 1, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(12, -31.639799, -60.672556, 0, '', 2, 11, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(13, -31.639799, -60.672610, 0, 'Aula 1 - Aula 2', 2, 1, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(14, -31.639969, -60.672544, 0, 'Escalera', 2, 5, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia)
VALUES(15, -31.639937, -60.671198, 0, '', 1, 11);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(16, -31.639651, -60.671193, 0, 'Aula 7', 4, 1, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia)
VALUES(17, -31.639710, -60.671893, 0, '', 1, 11);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(18, -31.639714, -60.671578, 0, '', 4, 11, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(19, -31.639811, -60.671578, 0, 'Aula 6', 4, 1, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(20, -31.639957, -60.672262, 1, 'Escalera', 2, 5, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(21, -31.639957, -60.672151, 1, '', 2, 11, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(22, -31.639957, -60.672071, 1, '', 2, 11, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(23, -31.639900, -60.672071, 1, 'Relaciones Institucionales', 2, 8, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(24, -31.639873, -60.672071, 1, 'Decanato', 2, 8, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(25, -31.639853, -60.672151, 1, 'Alumnado', 2, 8, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(26, -31.639799, -60.672151, 1, 'Mesa de Entradas', 2, 8, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(27, -31.639773, -60.672151, 1, 'Baños', 2, 2, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(28, -31.639773, -60.672198, 1, 'Laboratorio Electronica', 2, 7, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(29, -31.639773, -60.672296, 1, 'Laboratorio 1 - 2', 2, 7, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(30, -31.639773, -60.672550, 1, 'Aula Vigil', 2, 1, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(31, -31.639847, -60.672550, 1, 'Laboratorio 3 - 4', 2, 7, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(32, -31.639956, -60.672550, 1, 'Escalera', 2, 5, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(33, -31.639956, -60.672302, 1, 'Bedelia', 2, 8, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(34, -31.639954, -60.672269, 2, 'Escalera', 2, 5, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(35, -31.639954, -60.672109, 2, 'Laboratorio Química', 2, 7, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(36, -31.639954, -60.672088, 2, 'Secretaria de Extensión', 2, 8, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(37, -31.639928, -60.672088, 2, 'Aula de Dibujo', 2, 1, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(38, -31.639928, -60.672072, 2, 'Baños', 2, 2, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(39, -31.639958, -60.672285, 3, 'Escalera', 2, 5, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(40, -31.639958, -60.672214, 3, 'Lab. Bacteriología e Inmunología', 2, 7, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(41, -31.639958, -60.672146, 3, 'Aula 9', 2, 1, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(42, -31.639958, -60.672085, 3, '', 2, 11, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(43, -31.639925, -60.672085, 3, 'Aula 10', 2, 1, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(44, -31.639869, -60.672085, 3, 'Cátedra Matemática', 2, 9, 1);

-- Lado FICH Fin

-- Lado FCBC

INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(45, -31.639969, -60.672706, 0, 'Fotocopiadora', 2, 6, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(46, -31.639969, -60.672792, 0, 'Lab. Química Analítica I - II', 2, 7, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(47, -31.639969, -60.672823, 0, 'Escalera', 2, 5, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(48, -31.639775, -60.672825, 0, 'Lab. Química Biológica', 2, 7, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(49, -31.639969, -60.672966, 0, 'Bedelia', 2, 8, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(50, -31.639969, -60.673062, 0, 'Mesa de entradas', 2, 8, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(51, -31.639773, -60.672587, 1, 'Aula 1.1 - 1.2 - 1.3', 2, 1, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(52, -31.639956, -60.672605, 1, 'Cátedra Matemática Gral', 2, 9, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(53, -31.639956, -60.672699, 1, 'Lab. Química Orgánica I - II', 2, 7, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(54, -31.639956, -60.672795, 1, 'Cátedra Química Orgánica', 2, 9, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(55, -31.639956, -60.672838, 1, 'Escalera', 2, 5, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(56, -31.639956, -60.672884, 1, 'Sala Informática', 2, 9, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(57, -31.639956, -60.672951, 1, 'Alumnado', 2, 8, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(58, -31.639843, -60.672838, 1, 'Baños', 2, 2, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(59, -31.639954, -60.672429, 2, 'Cátedra Química Analítica I', 2, 9, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(60, -31.639863, -60.672429, 2, 'Aula 2.11', 2, 1, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(61, -31.639777, -60.672427, 2, 'Aula 2.5', 2, 1, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(62, -31.639777, -60.672326, 2, 'Aula 2.6 - 2.7', 2, 1, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(63, -31.639777, -60.672232, 2, 'Aula 2.8 - 2.9', 2, 1, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(64, -31.639777, -60.672154, 2, 'Aula 2.10', 2, 1, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(65, -31.639954, -60.672553, 2, 'Escalera', 2, 5, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(66, -31.639954, -60.672838, 2, 'Lab. Química General e Inorgánica', 2, 7, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(67, -31.639840, -60.672553, 2, 'Aula 2.12', 2, 1, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(68, -31.639777, -60.672553, 2, 'Aula 2.4', 2, 1, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(69, -31.639777, -60.672575, 2, 'Aula 2.2 - 2.3', 2, 1, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(70, -31.639777, -60.672614, 2, 'Aula 2.1', 2, 1, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(71, -31.639954, -60.672735, 2, 'Escalera', 2, 5, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(72, -31.639954, -60.672838, 2, 'Cátedra Química General e Inorgánica', 2, 9, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(73, -31.639833, -60.672838, 2, 'Baños', 2, 2, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(74, -31.639774, -60.672838, 2, 'Aula 2.13', 2, 1, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(75, -31.639958, -60.672361, 3, 'Lab.Bacteriología e Inmunología', 2, 7, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(76, -31.639958, -60.672432, 3, 'Biblioteca FICH/FCBC/ESS', 2, 12, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(77, -31.639958, -60.672493, 3, 'Lab Morfología Normal', 2, 7, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(78, -31.639958, -60.672555, 3, 'Escalera', 2, 5, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(79, -31.639958, -60.672729, 3, 'Escalera', 2, 5, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(80, -31.639958, -60.672839, 3, 'Lab. Microbiología General y Micología', 2, 7, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(81, -31.639846, -60.672839, 3, 'Baños', 2, 2, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(82, -31.639958, -60.672926, 3, 'Decanato', 2, 8, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(83, -31.639645, -60.671578, 0, 'Escalera', 4, 5, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(84, -31.639646, -60.671577, 1, 'Escalera', 4, 5, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(85, -31.639720, -60.671577, 1, '', 4, 11, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(86, -31.639720, -60.671650, 1, 'Laboratorio 5', 4, 7, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(87, -31.639969, -60.673159, 0, 'Entrada FCBC', 2, 4, 2);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(88, -31.639969, -60.673288, 0, '', 1, 11, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(89, -31.640159, -60.673288, 0, 'Entrada FADU ', 5, 4, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(90, -31.639964, -60.673159, 0, '', 1, 11, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(91, -31.639656, -60.673288, 0, '', 1, 11, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(92, -31.639649, -60.672556, 0, '', 1, 11, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(93, -31.639649, -60.671893, 0, '', 1, 11, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(94, -31.639567, -60.671893, 0, '', 1, 11, 1);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(95, -31.639567, -60.670975, 0, 'Cantina', 1, 3, 3);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(96, -31.639567, -60.670850, 0, 'Entrada FCM', 1, 4, 3);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(97, -31.639937, -60.670975, 0, '', 1, 11, 1);

-- Lado FCBC Fin 

-- Lado FADU

INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(98, -31.640159, -60.673362, 0, '', 5, 11, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(99, -31.640210, -60.673362, 0, 'Bedelia', 5, 8, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(100, -31.640128, -60.673362, 0, '', 5, 11, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(101, -31.640128, -60.673395, 0, 'Cantina', 5, 3, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(102, -31.640128, -60.673532, 0, '', 5, 11, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(103, -31.640071, -60.673532, 0, '', 5, 11, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(104, -31.640071, -60.673592, 0, 'Libreria', 5, 9, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(105, -31.640071, -60.673658, 0, 'Salón de Actos', 5, 9, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(106, -31.640128, -60.673572, 0, 'Escalera', 5, 5, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(107, -31.640194, -60.673572, 0, 'Baños', 5, 2, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(108, -31.640119, -60.673602, 1, 'Escalera', 5, 5, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(109, -31.640181, -60.673602, 1, 'Baños', 5, 2, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(110, -31.640067, -60.673602, 1, '', 5, 11, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(111, -31.640044, -60.673602, 1, 'Área Administrativa', 5, 8, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(112, -31.640044, -60.673646, 1, 'Área Gestión', 5, 8, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(113, -31.640067, -60.673474, 1, 'Asuntos Estudiantiles', 5, 8, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(114, -31.640067, -60.673350, 1, '', 5, 11, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(115, -31.640041, -60.673350, 1, 'Alumnado', 5, 8, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(116, -31.640116, -60.673608, 2, 'Escalera', 5, 5, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(117, -31.640178, -60.673608, 2, 'Baños', 5, 2, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(118, -31.640116, -60.673567, 2, 'Sala Informática 1', 5, 1, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(119, -31.640070, -60.673608, 2, 'Aulas Especiales', 5, 1, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(120, -31.640070, -60.673802, 2, 'Secretaria de Posgrado', 5, 8, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(121, -31.640070, -60.673421, 2, 'Sala Informática 2', 5, 1, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(122, -31.640070, -60.673336, 2, '', 5, 11, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(123, -31.640003, -60.673336, 2, 'Biblioteca FADU/FHUC/ISM', 5, 12, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(124, -31.640117, -60.673607, 3, 'Escalera', 5, 5, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(125, -31.640175, -60.673607, 3, 'Baños', 5, 2, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(126, -31.640250, -60.673607, 3, '', 5, 11, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(127, -31.640250, -60.673747, 3, 'Taller 4', 5, 10, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(128, -31.640117, -60.673577, 3, 'Fotocopiadora', 5, 6, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(129, -31.640073, -60.673607, 3, 'Taller 3', 5, 10, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(130, -31.640073, -60.673638, 3, 'Taller 1', 5, 10, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(131, -31.640073, -60.673553, 3, 'Taller 2', 5, 10, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(132, -31.640113, -60.673581, 4, 'Escalera', 5, 5, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(133, -31.640174, -60.673581, 4, 'Baños', 5, 2, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(134, -31.640252, -60.673581, 4, 'Taller 8', 5, 10, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(135, -31.640252, -60.673533, 4, 'Taller 9', 5, 10, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(136, -31.640252, -60.673744, 4, 'Taller 7', 5, 10, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(137, -31.640069, -60.673581, 4, '', 5, 11, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(138, -31.640069, -60.673606, 4, 'Taller 5', 5, 10, 4);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(139, -31.640069, -60.673531, 4, 'Taller 6', 5, 10, 4);

-- Lado FADU Fin

-- Lado FHUC

INSERT INTO Punto(idPunto, latitud, longitud, piso, nombre, tieneImagen, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(140, -31.64021, -60.673491, 0, 'Fotocopiadora', 0, 5, 6, 5);
INSERT INTO Punto(idPunto, latitud, longitud, piso, nombre, tieneImagen, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(141, -31.640194, -60.673491, 0, '', 0, 5, 11, 5);
INSERT INTO Punto(idPunto, latitud, longitud, piso, nombre, tieneImagen, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(142, -31.640272, -60.673572, 0, 'Aula 3', 0, 5, 1, 5);
INSERT INTO Punto(idPunto, latitud, longitud, piso, nombre, tieneImagen, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(143, -31.640272, -60.673496, 0, 'Aula 2', 0, 5, 1, 5);
INSERT INTO Punto(idPunto, latitud, longitud, piso, nombre, tieneImagen, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(144, -31.640272, -60.673399, 0, 'Aula 1', 0, 5, 1, 5);
INSERT INTO Punto(idPunto, latitud, longitud, piso, nombre, tieneImagen, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(145, -31.640272, -60.673692, 0, 'Aula 4', 0, 5, 6, 5);
INSERT INTO Punto(idPunto, latitud, longitud, piso, nombre, tieneImagen, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(146, -31.640272, -60.673726, 0, 'Aula 5 - Aula 8', 0, 5, 1, 5);
INSERT INTO Punto(idPunto, latitud, longitud, piso, nombre, tieneImagen, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(147, -31.640272, -60.673818, 0, 'Aula 6 - Aula 7', 0, 5, 1, 5);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(148, -31.640260, -60.673602, 1, '', 5, 11, 5);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(149, -31.640260, -60.673821, 1, 'Alumnado', 5, 8, 5);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(150, -31.640307, -60.673821, 1, 'Mesa de Entradas', 5, 8, 5);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(151, -31.640307, -60.673769, 1, 'Secretaría Administrativa', 5, 8, 5);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(152, -31.640307, -60.673559, 1, 'Decanato', 5, 8, 5);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(153, -31.640260, -60.673559, 1, '', 5, 11, 5);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(154, -31.640225, -60.673608, 2, '', 5, 11, 5);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(155, -31.640261, -60.673608, 2, '', 5, 11, 5);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(156, -31.640261, -60.673658, 2, 'Aula 12', 5, 1, 5);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(157, -31.640261, -60.673722, 2, 'Aula 11', 5, 1, 5);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(158, -31.640261, -60.673745, 2, 'Aula 9', 5, 1, 5);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(159, -31.640261, -60.673834, 2, 'Aula 10', 5, 1, 5);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(160, -31.640225, -60.673427, 2, '', 5, 11, 5);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(161, -31.640272, -60.673427, 2, 'Aula 14', 5, 1, 5);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(162, -31.640258, -60.673537, 3, 'Laboratorio 1', 5, 7, 5);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(163, -31.640258, -60.673425, 3, 'Laboratorio 2', 5, 7, 5);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(164, -31.640258, -60.673400, 3, 'Laboratorio 3', 5, 7, 5);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(165, -31.640258, -60.673363, 3, 'Laboratorio Fisico Química', 5, 7, 5);

-- Lado FHUC Fin

-- Aulario

INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(166, -31.639964, -60.674034, 0, 'Entrada Aulario Común', 6, 4, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(167, -31.639964, -60.674178, 0, '', 6, 11, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(168, -31.640038, -60.674178, 0, 'Bedelia', 6, 8, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(169, -31.640038, -60.674210, 0, 'Libreria', 6, 9, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(170, -31.640038, -60.674118, 0, '', 6, 11, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(171, -31.640014, -60.674118, 0, 'Escalera', 6, 5, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(172, -31.640006, -60.674119, 1, 'Escalera', 6, 5, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(173, -31.640039, -60.674119, 1, '', 6, 11, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(174, -31.640039, -60.674212, 1, 'Aula 3', 6, 1, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(175, -31.640039, -60.674253, 1, 'Aula 2', 6, 1, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(176, -31.640039, -60.674292, 1, 'Aula 1', 6, 1, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(177, -31.640006, -60.674123, 2, 'Escalera', 6, 5, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(178, -31.640061, -60.674077, 2, 'Baños', 6, 2, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(179, -31.639938, -60.674123, 2, 'Taller A', 6, 10, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(180, -31.640061, -60.674123, 2, 'Taller B', 6, 10, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(181, -31.640065, -60.674119, 1, '', 6, 11, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(182, -31.640065, -60.674073, 1, 'Baños', 6, 2, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(183, -31.640064, -60.674118, 0, '', 6, 11, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(184, -31.640064, -60.674073, 0, 'Baños', 6, 2, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(185, -31.640002, -60.674120, 3, 'Escalera', 6, 5, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(186, -31.639940, -60.674120, 3, 'Taller C', 6, 10, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(187, -31.640064, -60.674120, 3, 'Aula 4', 6, 1, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(188, -31.640064, -60.674073, 3, 'Baños', 6, 2, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(189, -31.640009, -60.674121, 4, 'Escalera', 6, 5, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(190, -31.640009, -60.674162, 4, 'Aula 5', 6, 1, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(191, -31.640009, -60.674216, 4, 'Aula 6', 6, 1, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(192, -31.640009, -60.674244, 4, 'Aula 7', 6, 1, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(193, -31.640009, -60.674308, 4, 'Aula 8', 6, 1, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(194, -31.640066, -60.674121, 4, '', 6, 11, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(195, -31.640066, -60.674074, 4, 'Baños', 6, 2, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(196, -31.640002, -60.674120, 5, 'Escalera', 6, 5, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(197, -31.639940, -60.674120, 5, 'Taller 9', 6, 10, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(198, -31.640064, -60.674120, 5, 'Aula 10', 6, 1, 7);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(199, -31.640064, -60.674073, 5, 'Baños', 6, 2, 7);

-- Aulario Fin

INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(200, -31.640159, -60.671913, 0, '', 1, 11, 1);

-- FCM Inicio

INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(201, -31.639575, -60.670721, 0, '', 3, 11, 3);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(202, -31.639498, -60.670719, 0, 'Baños', 3, 2, 3);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(203, -31.639468, -60.670719, 0, 'Alumnado', 3, 8, 3);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(204, -31.639575, -60.670687, 0, 'Aula 1', 3, 1, 3);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(205, -31.639575, -60.670610, 0, 'Aula 2', 3, 1, 3);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(206, -31.639575, -60.670579, 0, 'Aula 5', 3, 1, 3);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(207, -31.639575, -60.670552, 0, 'Aula 3', 3, 1, 3);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(208, -31.639575, -60.670511, 0, 'Aula 4', 3, 1, 3);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(209, -31.639575, -60.670476, 0, 'Sala Informática', 3, 9, 3);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(210, -31.639575, -60.670429, 0, 'Sala Videoconferencia', 3, 9, 3);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(211, -31.639575, -60.670321, 0, '', 3, 11, 3);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(212, -31.639477, -60.670321, 0, 'Lab. Microscopia', 3, 7, 3);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(213, -31.639383, -60.670320, 0, 'Lab. de Habilidades', 3, 7, 3);
INSERT INTO Punto (idPunto, latitud, longitud, piso, nombre, idEdificio, idTipoDependencia, idUnidadAcademica)
VALUES(214, -31.639383, -60.670445, 0, 'Zona Boxes', 3, 9, 3);

-- FCM Fin

