INSERT INTO Limites (nombre, latitudEsqInfIzq, longitudEsqInfIzq, latitudEsqSupDer, longitudEsqSupDer) 
VALUES("Ciudad Universitaria", -31.641036, -60.674475, -31.639325, -60.670192);
INSERT INTO Limites (nombre, latitudEsqInfIzq, longitudEsqInfIzq, latitudEsqSupDer, longitudEsqSupDer) 
VALUES("FICH/FBCB/ESS", -31.640064, -60.673090, -31.639671, -60.671973);
INSERT INTO Limites (nombre, latitudEsqInfIzq, longitudEsqInfIzq, latitudEsqSupDer, longitudEsqSupDer) 
VALUES("FCM", -31.639886, -60.670827, -31.639312, -60.670215);
INSERT INTO Limites (nombre, latitudEsqInfIzq, longitudEsqInfIzq, latitudEsqSupDer, longitudEsqSupDer) 
VALUES("NAVE FICH", -31.639896, -60.671735, -31.639576, -60.670991);
INSERT INTO Limites (nombre, latitudEsqInfIzq, longitudEsqInfIzq, latitudEsqSupDer, longitudEsqSupDer) 
VALUES("FADU/FHUC", -31.640346, -60.673949, -31.639980, -60.673311);
INSERT INTO Limites (nombre, latitudEsqInfIzq, longitudEsqInfIzq, latitudEsqSupDer, longitudEsqSupDer) 
VALUES("Aulario", -31.640131, -60.674323, -31.639922, -60.674045);


INSERT INTO Edificio (idInternoEdificio, nombre, cantidadPisos, idLimites)
VALUES(1, "Ciudad Universitaria", 1, 1);
INSERT INTO Edificio (idInternoEdificio, nombre, cantidadPisos, idLimites)
VALUES(2, "FICH/FCBC/ESS", 4, 2);
INSERT INTO Edificio (idInternoEdificio, nombre, cantidadPisos, idLimites)
VALUES(3, "FCM", 1, 3);
INSERT INTO Edificio (idInternoEdificio, nombre, cantidadPisos, idLimites)
VALUES(4, "NAVE FICH", 2, 4);
INSERT INTO Edificio (idInternoEdificio, nombre, cantidadPisos, idLimites)
VALUES(5, "FADU/FHUC", 5, 5);
INSERT INTO Edificio (idInternoEdificio, nombre, cantidadPisos, idLimites)
VALUES(6, "Aulario", 6, 6);
