CREATE
	TABLE Limites(
		idLimite INTEGER UNIQUE AUTO_INCREMENT NOT NULL,
		nombre VARCHAR(50) NOT NULL,
		latitudEsqInfIzq DOUBLE NOT NULL,
		longitudEsqInfIzq DOUBLE NOT NULL,
		latitudEsqSupDer DOUBLE NOT NULL,
		longitudEsqSupDer DOUBLE NOT NULL,
		PRIMARY KEY (idLimite)
	);

CREATE
	TABLE Edificio(
		idEdificio INTEGER UNIQUE AUTO_INCREMENT NOT NULL,
		idInternoEdificio INTEGER UNIQUE NOT NULL,
		nombre VARCHAR(50) NOT NULL,
		cantidadPisos INTEGER NOT NULL,
		idLimites INTEGER NOT NULL,
		PRIMARY KEY (idEdificio),
        	FOREIGN KEY (idLimites) REFERENCES Limites (idLimite)
	);


CREATE
	TABLE Unidad_Academica(
		idUnidadAcademica INTEGER UNIQUE AUTO_INCREMENT NOT NULL,
		nombre VARCHAR(50) NOT NULL
	);
	
CREATE
	TABLE Tipo_Dependencia(
		idTipoDependencia INTEGER UNIQUE AUTO_INCREMENT NOT NULL,
		descripcion VARCHAR(50),
		esVisible BIT(1) NOT NULL,
		PRIMARY KEY (idTipoDependencia)
	);
	
CREATE
	TABLE Punto(
		idPunto INTEGER UNIQUE NOT NULL,
		latitud DOUBLE NOT NULL,
		longitud DOUBLE NOT NULL,
		piso INTEGER NOT NULL,
		nombre VARCHAR(50) NOT NULL,
		tieneImagen BIT(1) DEFAULT 0,
		idEdificio INTEGER NOT NULL,
		idTipoDependencia INTEGER NOT NULL,
		idUnidadAcademica INTEGER,
		PRIMARY KEY (idPunto),
        	FOREIGN KEY (idEdificio) REFERENCES Edificio (idInternoEdificio),
		FOREIGN KEY (idTipoDependencia) REFERENCES Tipo_Dependencia (idTipoDependencia),
		FOREIGN KEY (idUnidadAcademica) REFERENCES Unidad_Academica (idUnidadAcademica)
	);
	
CREATE
	TABLE Conexiones(
		idPuntoA INTEGER NOT NULL,
		idPuntoB INTEGER NOT NULL,
		PRIMARY KEY (idPuntoA, idPuntoB),
		FOREIGN KEY (idPuntoA) REFERENCES Punto (idPunto),
		FOREIGN KEY (idPuntoB) REFERENCES Punto (idPunto)
	);
